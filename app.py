from flask import Flask
from flask import jsonify
from flask import send_file
from flask_cors import CORS, cross_origin

from download_image import download_image
from textapply import apply_text
from blackwhite import black_and_white
from blur import gaussian_blur

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS']="Content-Type"


@app.route("/api/<text>/<blur>/<bw>/<image_id>/<path:filename_url>")
@cross_origin()
def generate_image(text="", blur="false", bw="false", image_id=1, filename_url="https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.talkwalker.com%2Fblog%2Fwhat-is-image-analysis&psig=AOvVaw0J_BIiJO2CSDC2EGbPuNXS&ust=1593636214466000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIC16NWzquoCFQAAAAAdAAAAABAD"):
    download_image(filename_url, image_id)
    apply_text(text, image_id)

    if bw == "true":
        black_and_white(image_id)

    if blur == "true":
        gaussian_blur(image_id)

    return "Success"


@app.route("/api/get/<path:image_id>")
@cross_origin()
def get_my_image(image_id="images/1.jpg"):
    if image_id == "images/undefined.jpg":
        return "Undefined"
    return send_file(image_id, mimetype="image/jpg")


@app.route("/test/download/<path:filenameurl>")
@cross_origin()
def download_rest_image(filenameurl):
    download_image(filenameurl)
    return jsonify("Success")


@app.route("/gen")
@cross_origin()
def hello():
    return jsonify("Hello World!")


@app.route("/gen/upload/image")
@cross_origin()
def upload_image():
    return "Uploading the image..."


@app.route("/gen/process/image_id/bottom_text/top_text")
@cross_origin()
def process_image():
    return "Processing image..."


if __name__ == "__main__":
    app.run()
