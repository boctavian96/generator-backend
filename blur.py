import cv2


def gaussian_blur(image_id):
    img = cv2.imread("images/" + image_id + ".jpg")

    blur = cv2.GaussianBlur(img, (5, 5), 0)

    cv2.imshow("Blur", blur)

    cv2.imwrite("images/" + image_id + ".jpg", blur)


def run():
    gaussian_blur()


#run()

