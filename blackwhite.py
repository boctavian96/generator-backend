import cv2


def black_and_white(image_id):

    original_image = cv2.imread("images/" + image_id + ".jpg")
    gray_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

    #(thresh, blackAndWhiteImage) = cv2.threshold(gray_image, 127, 255, cv2.THRESH_BINARY)

    #cv2.imshow('Black white image', blackAndWhiteImage)
    #cv2.imshow('Original image', original_image)
    #cv2.imshow('Gray image', gray_image)

    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    print("Creating the black image")
    cv2.imwrite("images/" + image_id + ".jpg", gray_image)


def run():
    black_and_white()

#run()